#include <iostream>
#include "fs.h"
#include <string>
#include <list>

namespace fs = boost::filesystem;

int main(int argc, char* argv[]) {
    if(argc < 2)
    {
        std::cerr << "Usage: " << argv[0] << " filepath" << std::endl;
        return -1;
    }

    fs::path corpusPath(argv[1]);
    if(fs::exists(corpusPath) == false) {
        std::cerr << "Error: The corpus path " << corpusPath << " wasn't found." << std::endl;
    }

    auto&& corpusSet = get_files_from_directory(corpusPath);

    for(auto document : corpusSet) {
        std::cout << document << std::endl;
    }

    return 0;
}