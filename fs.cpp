//
// Created by Florian Scholz on 01.06.17.
//
#include "fs.h"

namespace fs = boost::filesystem;

std::list<fs::path> get_files_from_directory(fs::path corpusPath) {
    std::list<fs::path> filePaths;

    fs::directory_iterator end_iterator;
    for(fs::directory_iterator fileIterator(corpusPath); fileIterator != end_iterator; fileIterator++) {
        auto documentPath = fileIterator->path();
        filePaths.push_back(documentPath);

    }

    return std::move(filePaths);
}