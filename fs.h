//
// Created by Florian Scholz on 01.06.17.
//

#ifndef INRP_FS_H
#define INRP_FS_H
#include <list>
#include <boost/filesystem.hpp>

/**
 * Gets all file paths from a directory
 * @param corpusPath The path of the directory
 * @return A stl list with all file paths
 */
std::list<boost::filesystem::path> get_files_from_directory(boost::filesystem::path corpusPath);

#endif //INRP_FS_H
